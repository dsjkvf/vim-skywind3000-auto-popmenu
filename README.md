vim-skywind3000-auto-popmenu
============================


## About

This is is fork of the auto-completion plugin [`vim-auto-popmenu`](https://github.com/skywind3000/vim-auto-popmenu/) originally written by [skywind3000](https://github.com/skywind3000/). I strongly encourage you to use the original plugin, and naturally, all the credits go to the original author (and the [original README](README.orig) is included for the reference).


## Changes

The only difference with the original plugin is that now it is enabled by default for any normal buffer no matter of its filetype.


## Installation

Copy the provided files to your `$VIMHOME` directory, or simply use any plugin manager available.


## Usage

    " set the completions sources
    set complete=.,k,w,b
    " or set it to complete from the current buffer only
    " set complet=.

    " always show the completion menu but don't select the first item
    set completeopt=menu,menuone,noselect

    " suppress system messages
    set shortmess+=c

Warning: will not work if [`paste`](http://vimdoc.sourceforge.net/htmldoc/options.html#'paste') is set.


## Options

    " remap <Tab>
    let g:apc_enable_tab = 1

    " start completion after the Nth character
    let g:apc_min_length = 1

    " ignore some keywords
    let g:apc_key_ignore = ['ZaphodBeeblebrox']

    " ignore some filetypes
    let g:apc_fts_ignore = {'asm': 1}
